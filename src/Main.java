import javax.swing.JOptionPane;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	
	public void recordHistory (String email) throws IOException
	{
		  
		RandomAccessFile writer = new RandomAccessFile("History.txt", "rw");
	    writer.seek(writer.length());
	    writer.writeChars(email+"\n");
	    writer.close();
	   
	    
//	    try {
//			String appConfigPath = "History.txt";
//			 //Opening the files
//			Properties appProps = new Properties();
//			appProps.load(new FileInputStream(appConfigPath));
//			appProps.setProperty("email", email);
//			appProps.store(new FileWriter(appConfigPath), null);
//			}catch(FileNotFoundException e)
//			{
//				JOptionPane.showMessageDialog(null,"no set up file!", "ERROR", JOptionPane.INFORMATION_MESSAGE,null);
//			}
	}
	
	public String readEmail() throws FileNotFoundException {
		
		String text="";
		try {
	//		JOptionPane.showMessageDialog(null,"scanning", "Email Sender", JOptionPane.INFORMATION_MESSAGE,null);	
			Scanner EmailTxt = new Scanner(new File("Email.txt"));
			while( EmailTxt.hasNextLine()) {
			 text+=EmailTxt.nextLine()+"\n";
			}
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			JOptionPane.showInputDialog(null,"No file found", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
			e.printStackTrace();
			return null;
		}
		
//		JOptionPane.showMessageDialog(null,text, "Email Sender", JOptionPane.INFORMATION_MESSAGE,null);
		return text;
		
	}
	
	
	public void sendEmail() throws IOException{
		
		String to = JOptionPane.showInputDialog(null,"Who do I email", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
		String[] choice0= {"Write an email", "Take email from the .txt file"};
		
		this.recordHistory(to);

		Object choiceInterface0= JOptionPane.showInputDialog(null, "What mode would you prefer?","Email Sender", JOptionPane.DEFAULT_OPTION,null, choice0, "Write an email");;
		
		
		String text = "" ;
		
		
		
		
		if(choiceInterface0.toString().equals(choice0[0])) {
			text = JOptionPane.showInputDialog(null,"What do I email", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
		}
		else if(choiceInterface0.toString().equals(choice0[1]))
		{
			
				text=readEmail();

				if (text.isEmpty())
				{
					JOptionPane.showInputDialog(null,"Nothing read", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
			
			
		}
		
		
		
		String topic = JOptionPane.showInputDialog(null,"What Subject", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
		String[] choice= {"spammer", "Send several times", "just an email"};
		Object choiceInterface= JOptionPane.showInputDialog(null, "What mode would you prefer?","Email Sender", JOptionPane.DEFAULT_OPTION,null, choice, "Send several times");
		
		
//		email mail= new email();
		
		
		if(choiceInterface.toString().equals(choice[0])) 
		{
			
			
		    try {
		        while (true) {
//		        	Calendar cal = Calendar.getInstance();
//		            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	//	        	String text= "Dear David,\n\nHope you are well and didn't forget the time.\nCurrently it is : "+ sdf.format(cal.getTime()) + "\n\nNever loose  track of time\n\n"+"Thank you,\n\nYour Conscience";
	//	        	String text =" ";
					new email(to, topic, text, 10);
		            Thread.sleep(1 * 200);
		        }
		    } catch (InterruptedException e) {
		        e.printStackTrace();
		    }
		    
		    
		}
		
		
		else if (choiceInterface.toString().equals(choice[1]))
		{
			
			
			int askedNumber = Integer.valueOf(JOptionPane.showInputDialog(null,"How many times?", "Email Sender", JOptionPane.INFORMATION_MESSAGE));
			
				new email(to, topic, text,askedNumber);
	        
		}
		
		else 
		{
			new email(to, topic, text);
		}
	}
	
	
	public void setUp() throws IOException {
		String name = JOptionPane.showInputDialog(null,"Name of the owner", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
		String emailAddress = JOptionPane.showInputDialog(null,"Email address", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
		String password = JOptionPane.showInputDialog(null,"Password", "Email Sender", JOptionPane.INFORMATION_MESSAGE);
		
		new email().recordEmailData(name, emailAddress, password);
		
	}
	
	
	
	
	public static void main (String [] Crs) throws IOException 
	{
		Main app= new Main();
		String[] setUp= {"Set up new address", "Send form the old one"};
		Object choiceSetUp= JOptionPane.showInputDialog(null, null,"Email Sender", JOptionPane.DEFAULT_OPTION,null, setUp, setUp[1]);
		if (choiceSetUp.equals(setUp[1])) {
			
			app.sendEmail();
		}
	else if (choiceSetUp.equals(setUp[0])) {
		app.setUp();
	}
		
	}
}